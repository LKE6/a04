#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#5-a


# In[3]:


class Student:
    def __init__(self, lastName="Popescu", gpa=3.8):
        if isinstance(lastName, str):
            self.lastName = lastName
        else:
            print("Warning: Invalid last name, setting to default value 'Popescu'")
            self.lastName = "Popescu"

        if isinstance(gpa, float) and 0.0 <= gpa <= 4.0:
            self.gpa = gpa
        else:
            print("Warning: Invalid GPA, setting to default value 3.8")
            self.gpa = 3.8

if __name__ == "__main__":
    # Test the Student class with some sample values
    s1 = Student("Doe", 3.6)
    s2 = Student("Smith", 5.0)
    s3 = Student(123, -1.0)

    print(s1.lastName, s1.gpa)
    print(s2.lastName, s2.gpa)
    print(s3.lastName, s3.gpa)


# In[ ]:





# In[4]:


#5-b


# In[5]:


class Student:
    def __init__(self, lastName="Popescu", gpa=3.8):
        if isinstance(lastName, str):
            self.lastName = lastName
        else:
            print("Warning: lastName must be a string. Setting to default value 'Popescu'.")
            self.lastName = "Popescu"
        if 0 <= gpa <= 4:
            self.gpa = gpa
        else:
            print("Warning: GPA must be between 0 and 4. Setting to default value of 3.8.")
            self.gpa = 3.8

    def compareGPA(self, other):
        if self.gpa > other.gpa:
            print(self.lastName)
        else:
            print(other.lastName)

if __name__ == "__main__":
    # Example usage
    s1 = Student("Smith", 3.9)
    s2 = Student("Johnson", 3.7)
    s3 = Student("Garcia", 4.5)  # Will print warning and set GPA to default value of 3.8
    s4 = Student(123, 2.5)  # Will print warning and set lastName to default value of 'Popescu'

    s1.compareGPA(s2)  # Will print 'Smith'
    s2.compareGPA(s3)  # Will print 'Garcia'
    s3.compareGPA(s4)  # Will print 'Garcia'


# In[ ]:





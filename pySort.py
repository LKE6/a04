#!/usr/bin/env python
# coding: utf-8

# In[1]:


def sorter(A):
    def partition(A, start, end):
        pivot_index = start
        for i in range(start, end):
            if A[i] < A[end]:
                A[i], A[pivot_index] = A[pivot_index], A[i]
                pivot_index += 1
        A[pivot_index], A[end] = A[end], A[pivot_index]
        return pivot_index

    def quicksort(A, start, end):
        if start < end:
            pivot_index = partition(A, start, end)
            quicksort(A, start, pivot_index - 1)
            quicksort(A, pivot_index + 1, end)

    quicksort(A, 0, len(A) - 1)


# In[2]:


# set the example


# In[4]:




# Define a list of numbers to be sorted
my_list = [5, 2, 8, 1, 7, 3]

# Call the sorter function to sort the list in-place
sorter(my_list)

# Print the sorted list
print(my_list)


# In[ ]:




